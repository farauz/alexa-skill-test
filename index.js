
const Alexa = require('ask-sdk-core');
var global_data = {
  "uid_child": "",
  "STATUS_CHILD": "",
  "INFO_RESPONSE":"",
  "INFO_CARD":"",
  "uid_parent": "",
  "name_child": ""
}

// let modev = 'https://faustino.myseduca.com'
let modev = 'https://demo.gsepty.com'

const LaunchRequestHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
  },
  async handle(handlerInput) {
    var accessToken = handlerInput.requestEnvelope.context.System.user.accessToken;
    //Bienvenida aleatoria.
    let welcome_msg = ["Bienvenido", "Bienvenido a Seduca","Hola de nuevo","Hola", "Dime"]
    let example_msg = ["dame mis mensajes", "cual es la informacion de pago de mis representados"," que hay en la cafetería para hoy","que hay en la cafetería para mañana", "dame el saldo de mi representados"]
    var randomNum = Math.floor(Math.random() * (welcome_msg.length))
    let speakOutput = welcome_msg[randomNum] + ', ';


    if(accessToken){
      await getRemoteDataByPost(modev+'/2/api/alexa/user/info?seduca=1', {"token": String(accessToken) },2000)
        .then((response) => {

          const result = JSON.parse(response);


          if (result.length != 0){

            for (let index = 0; index < result.length; index++) {
              const element = result[index];
              randomNum = Math.floor(Math.random() * (example_msg.length))
              speakOutput += ` ${(element.name).split(' ')[0]}!. Prueba a decir: ${example_msg[randomNum]}`;

              randomNum = Math.floor(Math.random() * (example_msg.length))

              speakOutput += ', ó, ' + example_msg[randomNum] + '. Ahora bien, En que puedo ayudarte?';

              global_data.uid_parent = element.uid
  
            }
  
          } else {
            speakOutput = "Tuve problemas obteniendo tu informacion... Intentalo denuevo!"
          }
  
  
        })
        .catch((err) => {
          speakOutput = `Tuve problemas: ${err.message}`;
        });
    }else{
      speakOutput += 'Necesitas vincular su cuenta de Seduca. Siga las instrucciones en la pantalla o en su aplicación Alexa.'

      return handlerInput.responseBuilder
      .speak(speakOutput)
      // .withStandardCard('Error!', 'Debes iniciar sesión para poder acceder a tus datos en Seduca.', 'https://seducacdn.b-cdn.net/images/escudo_text_seduca_small.png','https://seducacdn.b-cdn.net/images/logoh3.png')
      .withLinkAccountCard()
      .withShouldEndSession(true)
      .getResponse();
    }


    return handlerInput.responseBuilder
      .speak(speakOutput)
      .withStandardCard("Bienvendio!","\r\n"+speakOutput,"https://seducacdn.b-cdn.net/images/logoh2.png","https://seducacdn.b-cdn.net/images/logoh3.png")
      .withShouldEndSession(false)
      .getResponse();
  }
};

const HelpIntentHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
      && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.HelpIntent';
  },
  handle(handlerInput) {
    const speakOutput = 'Estás en la sección de ayuda de Seduca Skill. Primero debes saber y tener presente, que su representante debe estar valído y activo en el colegio al que esté afiliado con SEDUCA. Ahora bien, los comando de lanzamientos son sencillos, en esta sesion de ayuda usaré a "Dorna" como mi representada. Tomemos de ejemplo: Si queremos obtener las asignaciones de "Dorna" para un dia en cuestion. En este caso solo tienes que decir: Alexa, que asignaciones tiene Dorna para hoy... Aqui hay 2 cosas importantes, la primera es, que puedes utilizar distintas acciones para llamar a un comando de lanzamiento. Ejemplo: Qué, Dime, Cuales y Dame. Con el tiempo Alexa irá aprendiendo a identificar cada una de esta acciones, asi que de eso no te preocupes. La segunda cosa importante del ejemplo es el dia en el que quieres las asignaciones, este puede ser en forma de día en específico, ejemplo: Lunes, martes o viernes. Tambien puede ser una fecha en específico, ejemplo: 14 de junio. Incluso, puede ser palabras como: Hoy, mañana, pasado mañana... De igual forma como el comando de lanzamiento que usamos de ejemplo, puedes preguntar u obtener informacion de tu representado como: informacion de pagos, Conferencias, Cafetería, Horarios, Mensajes propios como representante. Sin embargo, si necesitas mayor informacion sobre que puedes y no puedes hacer con esta skill, consulta a Soporte del colegio en cuestion.';

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .withShouldEndSession(false)
      .getResponse();
  }
};

const CancelAndStopIntentHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
      && (Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.StopIntent' || Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.CancelIntent');
  },
  handle(handlerInput) {
    const speakOutput = 'Hasta pronto. Solo recuerda decir: Alexa, abre SEDUCA.';

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .getResponse();
  }
};

const ConferenceInfoIntentHandler = {

  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
      && Alexa.getIntentName(handlerInput.requestEnvelope) === 'ConferenceInfoIntent';
  },
  async handle(handlerInput) {
    let outputSpeech = 'No tienes ninguna reunion para hoy! ';//https://demo.gsepty.com/2/app_dev.php/api/user/411/2/conference?seduca=1

    await getRemoteDataByPost('https://demo.gsepty.com/2/app_dev.php/api/user/411/2/conference?seduca=1', {})
      .then((response) => {
        const result = JSON.parse(response);

        if (result.message) {
          outputSpeech = `Tuve problemas obteniendo las reuniones, no te identifico en SEDUCA... `
        }


        if (result.length < 1) {
          outputSpeech = 'No tienes ninguna reunion para hoy! ';
        } else {
          outputSpeech = `Tienes ${result.length} reuniones... `

          for (let index = 0; index < result.length; index++) {
            const element = result[index];
            outputSpeech += element.subject + " desde las " + element.start + " hasta las " + element.end + "... ";
          }
        }


      })
      .catch((err) => {
        speakOutput = `Tuve problemas obteniendo las reuniones. ${err.message}`;
      });

    return handlerInput.responseBuilder
      .speak(outputSpeech)
      .getResponse();
  }
};



//ASSIGNMENTS
const GuardianAssignmentsByStudentIntentHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
      && Alexa.getIntentName(handlerInput.requestEnvelope) === 'GuardianAssignmentsByStudentIntent';
  },
  async handle(handlerInput) {
    const currentIntent = handlerInput.requestEnvelope.request.intent;
    // const upsServiceClient = handlerInput.serviceClientFactory.getUpsServiceClient();
    const firstname = currentIntent.slots.firstname_child.value;
    const date = currentIntent.slots.date.value;

    let speakOutput = "Tuve problema con la consulta, hable con soporte.";

    if (firstname.length != 0) { // EL NOMBRE DE ENTRADA
      global_data.name_child = firstname.charAt(0).toUpperCase() + firstname.slice(1) 
      if(date){
        getAssginmentByName(date); //
      }else{
        getAssginmentByName(); //
      }

      setTimeout(() => {
        if (global_data.STATUS_CHILD === "success") {
          speakOutput = global_data.INFO_RESPONSE;
        } else {
          speakOutput = "Lo siento, la informacion del estudiante no pudo ser obtenida con exito."
        }


        return handlerInput.responseBuilder
          .speak(speakOutput)
          .withStandardCard("Asignaciones", global_data.INFO_CARD,"https://seducacdn.b-cdn.net/images/logoh2.png","https://seducacdn.b-cdn.net/images/logoh3.png")
          .withShouldEndSession(true)
          .getResponse();


      }, 2000)

    }


    return handlerInput.responseBuilder
      .speak(speakOutput)
      .withShouldEndSession(true)
      .getResponse();

  }
}

const getAssginmentByName = function (date) {

  getRemoteDataByPost(modev+'/2/app_dev.php/api/alexa/guardian/'+global_data.uid_parent+'/child/'+global_data.name_child+'/assignment/?seduca=1', {dates:date}, 1500)
    .then((response) => {
      const result = JSON.parse(response);

      if (result.message) {
        global_data.STATUS_CHILD = `Tuve problemas obteniendo la informacion del estudiantes. No te identifico en SEDUCA... `
      }
      global_data.STATUS_CHILD = "success"

      if(result.length != 0){
        global_data.INFO_CARD = `${result[0].name} tiene ${result.length} asignaciones.`
        global_data.INFO_RESPONSE = `${result[0].name} tiene ${result.length} asignaciones. En breve las menciono. Recuerda que para detenerme, solo dí: Alexa, detente.`;
  
        for (let index = 0; index < result.length; index++) {
          const element = result[index];
          global_data.INFO_RESPONSE += `
            Asignación #${index+1}: ${element.name_assignment}.
            El curso o Materia es ${element.subject} y es dictada por el profesor: ${element.teacher}.
            Esta asignación es para el día ${element.date}.
          `;
        }
      }else{
        if(date){
          global_data.INFO_CARD = `${global_data.name_child} No tiene asignaciones vigentes para el dia ${date}.`
          global_data.INFO_RESPONSE = `${global_data.name_child} No tiene asignaciones vigentes para el dia ${date}.`
          
        }else{
          global_data.INFO_CARD = `${global_data.name_child} No tiene asignaciones hoy.`
          global_data.INFO_RESPONSE = `${global_data.name_child} No tiene asignaciones hoy.`
        }
      }


    }).catch((err) => {
      global_data.STATUS_CHILD = `${date} Tuve problemas obteniendo la informacion del estudiantes. Revisando la respuesta de la peticion es que: ${err.message}`;
    });

}
//ASSIGNMENTS




//PAYMENTS
const PaymentsByGuardianIntentHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
      && Alexa.getIntentName(handlerInput.requestEnvelope) === 'PaymentsByGuardianIntent';
  },
  async handle(handlerInput) {
    const currentIntent = handlerInput.requestEnvelope.request.intent;
    const firstname = currentIntent.slots.firstname_child.value;

    global_data.INFO_RESPONSE = ""; //Reseteo la informacion de salida

    let speakOutput = "Tuve problema con la consulta, hable con soporte.";
    let title_card = "Informacion de pago";

    if ( typeof firstname != "undefined" ) { // EL NOMBRE DE ENTRADA
      global_data.name_child = firstname.charAt(0).toUpperCase() + firstname.slice(1) 
    }else{
      global_data.name_child = 'a'
    }
      getPaymentsByStudent(); //

      setTimeout(() => {
        if (global_data.STATUS_CHILD === "success") {
          speakOutput = global_data.INFO_RESPONSE;
        } else {
          speakOutput =  global_data.STATUS_CHILD;
        }

        return handlerInput.responseBuilder
          .speak(speakOutput)
          .withStandardCard(title_card,global_data.INFO_CARD,"https://seducacdn.b-cdn.net/images/logoh2.png","https://seducacdn.b-cdn.net/images/logoh3.png")
          .withShouldEndSession(true)
          .getResponse();


      }, 2000)

    // }

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .withShouldEndSession(true)
      .getResponse();

  }
}

const getPaymentsByStudent = function() {
  const url = modev+'/2/api/alexa/guardian/'+global_data.uid_parent+'/child/'+global_data.name_child+'/payments/?seduca=1';
  getRemoteDataByPost(url,{}, 2000)
    .then((response) => {
        const result = JSON.parse(response);

        if (result.message) {
          global_data.STATUS_CHILD = `Tuve problemas obteniendo la informacion del estudiantes. No te identifico en SEDUCA... `
        }

        global_data.STATUS_CHILD = "success"

        if(result.length != 0){

          for (let index = 0; index < result.length; index++) {
            const element = result[index];
            
            if (typeof element.balance != 'undefined') {
              var monto_dolares = element.balance.split('.',2)[0]
              var monto_centavos = element.balance.split('.',2)[1]
              
              if( monto_dolares != 0 ){
                global_data.INFO_CARD += `La informacion de pago para ${element.name} es de: ${element.balance}$.\n\n`;
                global_data.INFO_RESPONSE += `La informacion de pago para ${element.name} es de: ${monto_dolares} dolares con ${monto_centavos} centavos.\n\n`;
              }else{
                global_data.INFO_CARD += `No hay informacion de pago para ${element.name}, esto quiere decir que su informacion de pago es 0.\n\n`;
                global_data.INFO_RESPONSE += `No hay informacion de pago para ${element.name}, esto quiere decir que su informacion de pago es 0.\n\n`;
              }
            }//
            
          }

        }else{
          global_data.INFO_RESPONSE = `${global_data.name_child} No tiene definida su informacion de pago, o un error fue lanzado en el proceso.\n\n`
        }


    }).catch((err) => {
      global_data.STATUS_CHILD = `Tuve problemas obteniendo la informacion del estudiantes. La respuesta de la peticion es de: ${err.message}`;
    });

}
//PAYMENTS




//MESSAGE
const MessageByUserIntentHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
      && Alexa.getIntentName(handlerInput.requestEnvelope) === 'MessageByUserIntent';
  },
  async handle(handlerInput) {
    //ALEXA DAME MIS MENSAJES
    let speakOutput = "Hubo un error al lanzar la peticion, contacte con SOPORTE.";


    getMessageByUser();


    setTimeout(() => {
      if (global_data.STATUS_CHILD === "success") {
        speakOutput = global_data.INFO_RESPONSE;
      } else {
        speakOutput = "Lo siento, la peticion fue muy prolongada, intentalo denuevo."
      }


      return handlerInput.responseBuilder
        .speak(speakOutput)
        .withStandardCard("Mensajes", global_data.INFO_CARD,"https://seducacdn.b-cdn.net/images/logoh2.png","https://seducacdn.b-cdn.net/images/logoh3.png")
        .withShouldEndSession(true)
        .getResponse();

    }, 2600)

    return handlerInput.responseBuilder
    .speak(speakOutput)
    .withShouldEndSession(true)
    .getResponse();

  }
}

const getMessageByUser= function() {

  getRemoteDataByPost(modev+'/2/app_dev.php/api/alexa/inbox/'+global_data.uid_parent+'/4/messages?seduca=1',{}, 2500)
    .then((response) => {
        const result = JSON.parse(response);
        var text_out = ""
        if (result.message) {
          global_data.STATUS_CHILD = `Tuve problemas obteniendo tu informacion. Intentalo denuevo.`
        }

        global_data.STATUS_CHILD = "success"


        if(result.length != 0){
          global_data.INFO_CARD = `Tienes ${result.length} mensajes por leer.\r\nPara saber el contenido del mensaje, abre SEDUCA y dirigete a la seccion de Mensajes.`;

          text_out = `Tienes ${result.length} mensajes por leer. En breve te los menciono. Solo recuerda, que si quieres saber el contenido del mensaje, abre SEDUCA y dirigete a la seccion de Mensajes. Por cierto, para detenerme solo dí, Alexa, detente. `
          for (let index = 0; index < result.length; index++) {
            const element = result[index];
            text_out += `
              Mensaje #${index+1}: Este mensaje fue enviado por: ${element.sender}, el dia ${element.date} y el asunto dice: ${countWords(element.subject)}.
            `;``

          }

          text_out += "Estos son todos los mensajes por leer que tienes, recuerda abrir SEDUCA para leer el contenido del mensaje. "
          global_data.INFO_RESPONSE = text_out;
        }else{
          global_data.INFO_RESPONSE = `No tienes mensajes sin leer.`
        }


    }).catch((err) => {
      global_data.STATUS_CHILD = `Tuve problemas obteniendo los mensajes. La respuesta de la peticion es de: ${err.message}`;
    });

}

function countWords(str) {
  const arr = str.split('RE:');
  return arr.filter(word => word !== '')[(arr.filter(word => word !== '').length -1)]
}
//MESSAGE





//CONFERENCE
const ConferenceByGuardianIntentHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
      && Alexa.getIntentName(handlerInput.requestEnvelope) === 'ConferenceByGuardianIntent';
  },
  async handle(handlerInput) {
    //ALEXA, QUE REUNIONES TIENE "NOMBRE DEL HIJO" PARA DIA X
    const currentIntent = handlerInput.requestEnvelope.request.intent;

    const firstname = currentIntent.slots.firstname_child.value;
    const date = currentIntent.slots.date.value;

    let speakOutput = "Hubo un error al lanzar la peticion, contacte con SOPORTE.";

    if (firstname.length != 0) { // EL NOMBRE DE ENTRADA
      global_data.name_child = firstname.charAt(0).toUpperCase() + firstname.slice(1) 
      if(date){
        getConferenceByName(date); //
      }else{
        getConferenceByName(); //
      }

      setTimeout(() => {
        if (global_data.STATUS_CHILD === "success") {
          speakOutput = global_data.INFO_RESPONSE;
        } else {
          speakOutput = "Lo siento, la peticion fue muy prolongada, "+global_data.STATUS_CHILD
        }
  
  
        return handlerInput.responseBuilder
          .speak(speakOutput)
          .reprompt(speakOutput)
          .withShouldEndSession(true)
          .getResponse();
  
      }, 2600)

    }

    return handlerInput.responseBuilder
    .speak(speakOutput)
    .withShouldEndSession(true)
    .getResponse();

  }
}//

const getConferenceByName = function (date) {

  getRemoteDataByPost(modev+`/2/app_dev.php/api/alexa/guardian/${global_data.uid_parent}/child/${global_data.name_child}/conference?seduca=1`, {dates:date}, 2500)
    .then((response) => {
      const result = JSON.parse(response);
      if (result.message) {
        global_data.STATUS_CHILD = `Tuve problemas obteniendo la informacion del estudiantes. Tus datos son erroneos.`
      }
      global_data.STATUS_CHILD = "success"

      if(result.length != 0){
        if(result.length == 1 ){
          global_data.INFO_RESPONSE = `Tienes solo ${result.length} reunion.
            Y es: ${result[0].subject}. Fue creada por ${result[0].host}, para el dia ${result[0].date} a las ${result[0].time}, con el fin de que ${result[0].description}.
          `;
          if(result[0].join == true){
            global_data.INFO_RESPONSE += `De hecho, ${global_data.name_child} Ya tiene que entrar a esta reunion!.`
          }

        }else{
          global_data.INFO_RESPONSE = `Tienes ${result.length} reuniones. En breve las menciono. Recuerda que para detenerme solo dí, Alexa, detente.`;
          for (let index = 0; index < result.length; index++) {
            const element = result[index];
            global_data.INFO_RESPONSE += `
              Reunion #${index+1}: ${element.subject}, fue creada por: ${element.host}, para el dia ${element.date} a las ${element.time}, con el fin de que ${element.description}.
            `;
            
            if(result.join == true){
              global_data.INFO_RESPONSE += `De hecho, ${global_data.name_child} Ya tiene que entrar a esta reunion!.`
            }
          }//for
        }
      }else{
        if(date){
          global_data.INFO_RESPONSE = `${global_data.name_child} No tiene reuniones vigentes para el dia ${date}.`
          
        }else{
          global_data.INFO_RESPONSE = `${global_data.name_child} No tiene reuniones hoy.`
        }
      }


    }).catch((err) => {
      global_data.STATUS_CHILD = `Tuve problemas obteniendo la informacion del estudiantes. Revisando la respuesta de la peticion es que: ${err.message}`;
    });

}
//CONFERENCE





//CAFETERIA
const CafeteriaByGuardianIntentHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
      && Alexa.getIntentName(handlerInput.requestEnvelope) === 'CafeteriaByGuardianIntent';
  },
  async handle(handlerInput) {

    const currentIntent = handlerInput.requestEnvelope.request.intent;

    const firstname = currentIntent.slots.firstname_child.value;
    const date = currentIntent.slots.date.value;

    let speakOutput = "Hubo un error al lanzar la peticion, contacte con SOPORTE.";

    if ( typeof firstname != "undefined" ) { // EL NOMBRE DE ENTRADA
      global_data.name_child = firstname.charAt(0).toUpperCase() + firstname.slice(1) 
    }else{
      global_data.name_child = 'a'
    }

    if(date){
      getCafeteriaByName(date); //
    }else{
      getCafeteriaByName(); //
    }

    setTimeout(() => {
      if (global_data.STATUS_CHILD === "success") {
        speakOutput = global_data.INFO_RESPONSE;
      } else {
        speakOutput = "Lo siento, la peticion fue muy prolongada, "+global_data.STATUS_CHILD
      }

      return handlerInput.responseBuilder
        .speak(speakOutput)
        .reprompt(speakOutput)
        .withShouldEndSession(true)
        .getResponse();

    }, 2800)

    return handlerInput.responseBuilder
    .speak(speakOutput)
    .withShouldEndSession(true)
    .getResponse();

  }
}//

const getCafeteriaByName = function (date) {

  getRemoteDataByPost(modev+`/2/app_dev.php/api/alexa/cafeteria/guardian/${global_data.uid_parent}/child/${global_data.name_child}/menu?seduca=1`, {dates:date}, 2700)
    .then((response) => {
      const result = JSON.parse(response);

      global_data.STATUS_CHILD = "success"

      if(result.length != 0){
        global_data.INFO_RESPONSE = `El menú para este día se ve interesante, en breve te las menciono. Recuerda hacer tus pedidos en la seccion de cafetería, y realizar los procesos necesarios para que su representado pueda adquirir su alimento en la cafetería correspondiente a su colegio. Por cierto, olvidé mencionar que para detenerme solo tienes que decir, Alexa, detente. `;

        for (let index = 0; index < result.length; index++) {
          const element = result[index];

          let monto_dolares = String(element.price).split('.',2)[0]
          let monto_centavos = String(element.price).split('.',2)[1]

          global_data.INFO_RESPONSE += `El ${index+1}º menú es ${element.title} `;

          if(element.description){
             global_data.INFO_RESPONSE += `y consiste en ${element.description}. `; 
          }

          global_data.INFO_RESPONSE += `. Este tiene un precio de ${monto_dolares} dolares con ${monto_centavos} centavos...`;

        }
        //º
      }else{
        if(date){
          global_data.INFO_RESPONSE = `No hay menú en la cafetería para el dia ${date}.`
        }else{
          global_data.INFO_RESPONSE = `No hay menú en la cafetería para hoy.`
        }
      }


    }).catch((err) => {
      global_data.STATUS_CHILD = `Tuve problemas obteniendo la informacion. Revisando la respuesta de la peticion es que: ${err.message}`;
    });

}
//CAFETERIA




//HORARIOS
const ScheduleByGuardianIntentHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
      && Alexa.getIntentName(handlerInput.requestEnvelope) === 'ScheduleByGuardianIntent';
  },
  async handle(handlerInput) {

    const currentIntent = handlerInput.requestEnvelope.request.intent;

    const firstname = currentIntent.slots.firstname_child.value;
    const date = currentIntent.slots.date.value;

    let speakOutput = "Hubo un error al lanzar la peticion, contacte con SOPORTE.";

    if ( typeof firstname != "undefined" ) { // EL NOMBRE DE ENTRADA
      global_data.name_child = firstname.charAt(0).toUpperCase() + firstname.slice(1) 
    }else{
      global_data.name_child = 'a'
    }

    if(date){
      getScheduleByName(date); //
    }

    setTimeout(() => {
      if (global_data.STATUS_CHILD === "success") {
        speakOutput = global_data.INFO_RESPONSE;
      } else {
        speakOutput = "Lo siento, la peticion fue muy prolongada, "+global_data.STATUS_CHILD
      }

      return handlerInput.responseBuilder
        .speak(speakOutput)
        .reprompt(speakOutput)
        .withShouldEndSession(true)
        .getResponse();

    }, 2800)

    return handlerInput.responseBuilder
    .speak(speakOutput)
    .withShouldEndSession(true)
    .getResponse();

  }
}//

const getScheduleByName = function (date) {

getRemoteDataByPost(modev+`/2/app_dev.php/api/alexa/guardian/${global_data.uid_parent}/child/${global_data.name_child}/schedule?seduca=1`, {dates:date}, 2700)
    .then((response) => {
      const result = JSON.parse(response);

      global_data.STATUS_CHILD = "success"

      if(result.length != 0){
        global_data.INFO_RESPONSE = `Para el dia ${date}, ${global_data.name_child} tiene lo siguiente... Recuerda, para detenerme solo dí: Alexa, detente... `;

        for (let index = 0; index < result.length; index++) {
          const element = result[index];

          global_data.INFO_RESPONSE += ` De ${element.time}, tiene el curso ${element.lessons[0].name}. Este es impartido por: ${element.lessons[0].teacher}...`;

        }
        //º
      }else{
        if(date){
          global_data.INFO_RESPONSE = `${global_data.name_child} no tiene clases el dia ${date}.`
        }
      }


    }).catch((err) => {
      global_data.STATUS_CHILD = `Tuve problemas obteniendo la informacion. Revisando la respuesta de la peticion es que: ${err.message}`;
    });

}
//HORARIOS


const getRemoteDataByPost = (url, body, time) => new Promise((resolve, reject) => {
  const options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Length': JSON.stringify(body).length,
    },
    timeout: time, // in ms
  }

  const client = require('https');

  const req = client.request(url, options, (response) => {

    const body = [];
    response.on('data', (chunk) => body.push(chunk));
    response.on('end', () => resolve(body.join('')));
  });
  req.on('error', (err) => reject(err));
  req.on('timeout', () => {
    req.destroy()
    reject(new Error('Request time out'))
  })

  req.write(JSON.stringify(body))
  req.end()
});

const YesIntentHandler = {
  canHandle(handlerInput) {
    // const attributes = handlerInput.attributesManager.getSessionAttributes();
    return handlerInput.requestEnvelope.request.type === 'IntentRequest'
      && handlerInput.requestEnvelope.request.intent.name === 'AMAZON.YesIntent'
      && handlerInput.attributesManager.getSessionAttributes().questionAsked === 'IsItSunnyOutToday';
  },
  handle(handlerInput) {
    // const requestAttributes = handlerInput.attributesManager.getRequestAttributes();
    const speakOutput = "";

    return handlerInput.responseBuilder
      .addDelegateDirective({
        name: 'GuardianAssignmentsByStudentIntent',
        confirmationStatus: 'NONEx',
        slots: {},
      })
      .speak(speakOutput)
      .getResponse();
  },
};

// const FallbackIntentHandler = {
//   canHandle(handlerInput) {
//     return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest'
//       && Alexa.getIntentName(handlerInput.requestEnvelope) === 'AMAZON.FallbackIntent';
//   },
  
//   handle(handlerInput) {
//     const currentIntent = handlerInput.requestEnvelope.request.intent;
//     const firstname = currentIntent.slots.firstname_child;

//     const speakOutput = 'Lo siento, no pude entenderte. Talves estoy teniendo problema en mi código.'

//     return handlerInput.responseBuilder
//       .speak(speakOutput)
//       .reprompt(speakOutput)
//       .getResponse();
//   }
// };

const SessionEndedRequestHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'SessionEndedRequest';
  },
  handle(handlerInput) {
    console.log(`~~~~ Session ended: ${JSON.stringify(handlerInput.requestEnvelope)}`);
    // Any cleanup logic goes here.
    return handlerInput.responseBuilder.getResponse(); // notice we send an empty response
  }
};

const IntentReflectorHandler = {
  canHandle(handlerInput) {
    return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
  },
  handle(handlerInput) {
    const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
    const speakOutput = `La peticion que acabas de hacer, No se encuentra disponible, pero si fue definida en los Intents de Alexa DEV. ${intentName}`;

    return handlerInput.responseBuilder
      .speak(speakOutput)
      //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
      .getResponse();
  }
};

const ErrorHandler = {
  canHandle() {
    return true;
  },
  handle(handlerInput, error) {
    const currentIntent = handlerInput.requestEnvelope.request.intent;
    const firstname = currentIntent.slots.firstname_child;

    const speakOutput = 'Lo siento, no pude entenderte. Talves estoy teniendo problema en mi código. ';
    
    console.log(`~~~~ Error handled: ${JSON.stringify(error)}`);

    return handlerInput.responseBuilder
      .speak(speakOutput)
      .reprompt(speakOutput)
      .getResponse();
  }
};





exports.handler = Alexa.SkillBuilders.custom()
  .addRequestHandlers(
    LaunchRequestHandler,
    // HelloWorldIntentHandler,
    HelpIntentHandler,
    YesIntentHandler,
    // ConferenceInfoIntentHandler,
    ConferenceByGuardianIntentHandler,
    MessageByUserIntentHandler,
    PaymentsByGuardianIntentHandler,
    GuardianAssignmentsByStudentIntentHandler,
    CafeteriaByGuardianIntentHandler,
    ScheduleByGuardianIntentHandler,
    CancelAndStopIntentHandler,
    //FallbackIntentHandler,
    SessionEndedRequestHandler,
    IntentReflectorHandler)
  .addErrorHandlers(
    ErrorHandler)
  .withCustomUserAgent(new Alexa.DefaultApiClient())
  .lambda();